# Desafio Suno

Suite de testes desenvolvida a partir da API da MovieDB. O foco da suite é conseguir validar o sucesso e a falha dos seguintes cenários:

1. Conseguir autenticar, criar e finalizar uma sessão após todo os testes.
2. Criar e excluir uma lista.
3. Buscar um filme, conseguir adicionar e remover da lista.
4. Classificar e remover classificação dos filmes.
5. Adicionar um filme como favorito.

Todas as chamadas criadas possuem testes específicos, sua estrutura foi organizada apartir pasta onde estava a chamada da API.

Link API: https://developers.themoviedb.org/3/getting-started/introduction

# Como importar a suíte

1. Faça donwload do arquivo **MovieDB - Suno Desafio.postman_collection**.
2. Abra o Postman.
3. Vá em **Import**, na aba **File** selecione o arquivo **MovieDB - Suno Desafio.postman_collection** e clique em **Import** e a suíte será configurada.  

# Como rodar a suíte

1. Crie um token na API da MovieDB, clique na pasta **MovieDB - Suno Desafio**, na aba **Auth** no campo **Value** coloque o token gerado. 
2. Rode manualmente a chamada **Success - Create Request Token** que está na pasta **Session > Get > Success**.
3. Pegue o id gerado no campo **request_token** e jogue na URL https://www.themoviedb.org/authenticate/coloque_token_aqui.
4. Permita que este token possa fazer alterações.
5. Passe o mouse em cima da pasta **MovieDB - Suno Desafio**, clique nos 3 pontos e clique em **Run colletion**.
6. Configure as chamadas na seguinte ordem (Não rodar a chamada **Success - Create Request Token**): 
- Success - Create Guest Session
- Fail - Create Session
- Success - Create Session
- Fail - Get Details (Account / Get / Fail)
- Success - Get Details (Account / Get / Success)
- Fail - Search Movies
- Success - Search Movies
- Fail - Create List
- Success - Create List
- Fail - Add Movie
- Success - Add Movie
- Success - Get Details (List / Get / Success)
- Success - Check Item Status
- Fail - Remove Movie
- Success - Remove Movie
- Fail - Add to Watchlist
- Success - Add to Watchlist
- Fail - Get Movie Watchlist
- Success - Get Movie Watchlist
- Fail - Mark as favorite
- Success - Mark as favorite
- Fail - Get Favorite Movies
- Success - Get Favorite Movies
- Fail - Rate Movie
- Success - Rate Movie
- Fail - Delete Rating
- Success - Delete Rating
- Fail - Delete Session
- Success - Delete List
- Success - Delete Session
7. Por fim, clique em **Run MovieDB - Suno Desafio**.
